FROM eclipse-temurin:21-jre-noble
LABEL maintainer "Naturalis <info@naturalis.nl>"

ARG WILDFLY_VERSION
ENV WILDFLY_VERSION ${WILDFLY_VERSION}
ENV DOWNLOAD_URL https://github.com/wildfly/wildfly/releases/download/${WILDFLY_VERSION}/wildfly-${WILDFLY_VERSION}.tar.gz
ENV DOWNLOAD_SHA1 https://github.com/wildfly/wildfly/releases/download/${WILDFLY_VERSION}/wildfly-${WILDFLY_VERSION}.tar.gz.sha1
ENV JBOSS_HOME /opt/jboss/wildfly

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Create a user and group used to launch processes
RUN groupadd -r jboss -g 1001 \
    && useradd -u 1001 -r -g jboss -m -d /opt/jboss -s /sbin/nologin -c "JBoss user" jboss \
    && chmod 755 /opt/jboss

USER root

RUN apt-get update && apt-get upgrade -y \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && curl -O -L "$DOWNLOAD_URL" \
    && SHA1=$(curl -L "$DOWNLOAD_SHA1") \
    && echo "$SHA1 wildfly-$WILDFLY_VERSION.tar.gz" | sha1sum --check --status - \
    && tar -xzf "wildfly-$WILDFLY_VERSION.tar.gz" \
    && mv "wildfly-$WILDFLY_VERSION" "$JBOSS_HOME" \
    && rm "wildfly-$WILDFLY_VERSION.tar.gz" \
    && chown -R jboss:0 "$JBOSS_HOME" \
    && chmod -R g+rw "$JBOSS_HOME"

# Ensure signals are forwarded to the JVM process correctly for graceful shutdown
ENV LAUNCH_JBOSS_IN_BACKGROUND true

# Set the working directory to jboss' user home directory
WORKDIR /opt/jboss

USER jboss

# Expose the ports in which we're interested
EXPOSE 8080

# Set the default command to run on boot
# This will boot WildFly in standalone mode and bind to all interfaces
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]
