# wildfly

## WildFly application server image

This image extends an Eclipse-Temurin JRE docker image and installs WildFly in the `/opt/jboss/wildfly` directory. The tag used provides information about the versions being used.


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`